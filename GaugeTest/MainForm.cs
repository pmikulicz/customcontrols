﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using CustomControls.Gauges.RadialGauges;
using AquaControls;

namespace GaugeTest
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
			comboBox1.Items.Add(CustomControls.Gauges.RadialGauges.DivisionStyle.Dotted);
			comboBox1.Items.Add(CustomControls.Gauges.RadialGauges.DivisionStyle.Linear);
			comboBox1.Items.Add(CustomControls.Gauges.RadialGauges.DivisionStyle.Triangular);

			comboBox2.Items.Add(CustomControls.Gauges.RadialGauges.DivisionStyle.Dotted);
			comboBox2.Items.Add(CustomControls.Gauges.RadialGauges.DivisionStyle.Linear);
			comboBox2.Items.Add(CustomControls.Gauges.RadialGauges.DivisionStyle.Triangular);

			comboBox5.Items.Add(CustomControls.Gauges.RadialGauges.RotationDirection.Anticlockwise);
			comboBox5.Items.Add(CustomControls.Gauges.RadialGauges.RotationDirection.Clockwise);

			comboBox3.Items.Add(Color.Orange);
			comboBox3.Items.Add(Color.Indigo);

		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			radialGauge1.DivisionStyle = (CustomControls.Gauges.RadialGauges.DivisionStyle)comboBox1.SelectedItem;
		}

		private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
		{
			radialGauge1.SubdivisionStyle = (CustomControls.Gauges.RadialGauges.DivisionStyle)comboBox2.SelectedItem;
		}

		private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
		{
			radialGauge1.FrontColor = (Color)comboBox3.SelectedItem;
		}

		private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
		{
			radialGauge1.CurrentValue = (float)comboBox4.SelectedItem;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Task.Factory.StartNew(() =>
			{
				for (var i = radialGauge1.MinValue; i <= radialGauge1.MaxValue; i++)
				{
					radialGauge1.CurrentValue = i;
					Thread.Sleep(50);
				}
			});
		}

		private void button2_Click(object sender, EventArgs e)
		{
			Task.Factory.StartNew(() =>
			{
				for (var i = radialGauge1.MaxValue; i >= radialGauge1.MinValue; i--)
				{
					radialGauge1.CurrentValue = i;
					Thread.Sleep(50);
				}
			});
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			radialGauge1.ScaleStartAngle = (float)numericUpDown1.Value;
		}

		private void numericUpDown2_ValueChanged(object sender, EventArgs e)
		{
			radialGauge1.ScaleStopAngle = (float)numericUpDown2.Value;
		}

		private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
		{
			radialGauge1.RotationDirection = (CustomControls.Gauges.RadialGauges.RotationDirection)comboBox5.SelectedItem;
		}

		private void numericUpDown3_ValueChanged(object sender, EventArgs e)
		{
			radialGauge1.Divisions = (int)numericUpDown3.Value;
		}

		private void numericUpDown4_ValueChanged(object sender, EventArgs e)
		{
			radialGauge1.Divisions = (int)numericUpDown4.Value;
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			radialGauge1.OuterDigits = checkBox1.Checked;
		}

		private void checkBox2_CheckedChanged(object sender, EventArgs e)
		{
			radialGauge1.InnerDigits = checkBox2.Checked;
		}
	}
}
