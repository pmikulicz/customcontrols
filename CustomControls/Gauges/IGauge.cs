﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CustomControls.Gauges
{
	interface IGauge
	{
		PointF Center { get; }
		PointF Pointer { get; }
		PointF[] Divisions { get; }
		PointF[] DivLinesStart { get; }
		PointF[] DivLinesEnd { get; }
		PointF[] DivTrianglesA { get; }
		PointF[] DivTrianglesB { get; }
		PointF[] Subdivisions { get; }
		PointF[] SubdivLinesStart { get; }
		PointF[] SubdivLinesEnd { get; }
		PointF[] SubdivTrianglesA { get; }
		PointF[] SubdivTrianglesB { get; }
		RectangleF OuterRectangle { get; }
		RectangleF MiddleRectangle { get; }
		RectangleF CenterRectangle { get; }
		float OuterThickness { get; }
		float PointerThickness { get; }
		float DigitsSize { get; }
		float DivisionThickness { get; }
		float DivisionLength { get; }
		float SubdivisionThickness { get; }
		float SubdivisionLength { get; }
		float[] DivisionValues { get; }

		PointF GetOuterDigitPos(int index, SizeF size);
		PointF GetInnerDigitPos(int index, SizeF size);
	}
}
