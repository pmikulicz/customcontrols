﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CustomControls.Gauges;

namespace CustomControls.Gauges.RadialGauges
{
	internal class Gauge : IGauge
	{
		#region Fields

		private const float outerRectDivisor = 1.05f;
		private const float middleRectDivisor = 1.55f;
		private const float middleRectOnlyInnerDivisor = 1.2f;
		private const float centerRectDivisor = 10f;
		private const float digitsOffsetDivisor = 35f;
		private const float digitsSizeDivisor = 25f;
		private const float outerThicknessDivisor = 50f;
		private const float pointerThicknessDivisor = outerThicknessDivisor;
		private const float divisionThicknessDivisor = 60f;
		private const float divisionLengthDivisor = 30f;
		private const float divisionTriangleAngleDivisor = 150f;
		private const float subdivisionThicknessDivisor = 400f;
		private const float subdivisionLengthDivisor = 80f;
		private const float subdivisionTriangleAngleDivisor = 300f;
		private const float straightAngel = 180f;
		private const float fullAngel = 2 * straightAngel;

		private readonly RadialGauge control;

		#endregion

		#region Constructors

		public Gauge(Control control)
		{
			this.control = control as RadialGauge;

			if (this.control == null)
				throw new ArgumentException("Passed object is not Speedometer instance");
		}

		#endregion

		#region Properties

		public PointF Center
		{
			get { return new PointF((float)control.Width / 2f, (float)control.Height / 2f); }
		}

		public PointF Pointer
		{
			get
			{
				// Compute pertcentage of current value relative to entire range of the scale.
				float percentage = (control.CurrentValue / (control.MaxValue - control.MinValue));
				// Compute stub-angle.
				float stubAngle = (control.ScaleStopAngle - control.ScaleStartAngle) * percentage;
				// Compute angle relative to start angle.
				float relativeAngle = control.ScaleStartAngle + stubAngle;

				return Transform(Center, (MiddleRectangle.Width / 2) + (DivisionLength / 2), relativeAngle);
			}
		}

		public PointF[] Divisions
		{
			get 
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				PointF[] points = new PointF[control.Divisions];

				for (int i = 0; i < points.Length; i++)
				{
					// Compute n angle relative to start angle.
					float relativeAngle = control.ScaleStartAngle + (divisonAngle * i);
					points[i] = Transform(Center, MiddleRectangle.Width / 2, relativeAngle);
				}

				return points;
			}
		}

		public PointF[] DivTrianglesA
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				PointF[] points = new PointF[control.Divisions];

				for (int i = 0; i < control.Divisions; i++)
				{
					// Compute n angle relative to start angle.
					float relativeAngle = divisonAngle * i + control.ScaleStartAngle;
					points[i] = Transform(Center, (MiddleRectangle.Width / 2) + (DivisionLength / 2), relativeAngle - (control.Width / divisionTriangleAngleDivisor));
				}

				return points;
			}
		}

		public PointF[] DivTrianglesB
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				PointF[] points = new PointF[control.Divisions];

				for (int i = 0; i < control.Divisions; i++)
				{
					// Compute n angle relative to start angle.
					float relativeAngle = divisonAngle * i + control.ScaleStartAngle;
					points[i] = Transform(Center, (MiddleRectangle.Width / 2) + (DivisionLength / 2), relativeAngle + (control.Width / divisionTriangleAngleDivisor));
				}

				return points;
			}
		}

		public PointF[] DivLinesStart
		{
			get 
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				PointF[] points = new PointF[control.Divisions];

				for (int i = 0; i < control.Divisions; i++)
				{
					// Compute n angle relative to start angle.
					float relativeAngle = divisonAngle * i + control.ScaleStartAngle;
					points[i] = Transform(Center, (MiddleRectangle.Width / 2) - (DivisionLength / 2), relativeAngle);
				}

				return points;
			}
		}

		public PointF[] DivLinesEnd
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				PointF[] points = new PointF[control.Divisions];

				for (int i = 0; i < control.Divisions; i++)
				{
					// Compute n angle relative to start angle.
					float relativeAngle = divisonAngle * i + control.ScaleStartAngle;
					points[i] = Transform(Center, (MiddleRectangle.Width / 2) + (DivisionLength / 2), relativeAngle);
				}

				return points;
			}
		}

		public PointF[] Subdivisions
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				// Compute one subdivision angle.
				float subdivisionAngle = divisonAngle / (control.Subdivisions);
				float relativeDivisionAngle = 0;
				float relativeSubdivisionAngle = 0;
				int index = 0;
				int size = ((control.Subdivisions - 1) * (control.Divisions - 1)) < 0 ? 0 : (control.Subdivisions - 1) * (control.Divisions - 1);
				PointF[] points = new PointF[size];

				for (int i = 0; i < control.Divisions - 1; i++)
				{
					// Compute n angle relative to start angle;
					relativeDivisionAngle = divisonAngle * i + control.ScaleStartAngle;

					for (int j = 1; j < control.Subdivisions; j++)
					{
						// Compute n angle relative to start angle.
						relativeSubdivisionAngle = relativeDivisionAngle + (j * subdivisionAngle);
						points[index] = Transform(Center, MiddleRectangle.Width / 2, relativeSubdivisionAngle);
						index++;
					}
				}

				return points;
			}
		}

		public PointF[] SubdivLinesStart
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				// Compute one subdivision angle.
				float subdivisionAngle = divisonAngle / (control.Subdivisions);
				float relativeDivisionAngle = 0;
				float relativeSubdivisionAngle = 0;
				int index = 0;
				int size = ((control.Subdivisions - 1) * (control.Divisions - 1)) < 0 ? 0 : (control.Subdivisions - 1) * (control.Divisions - 1);
				PointF[] points = new PointF[size];

				for (int i = 0; i < control.Divisions - 1; i++)
				{
					// Compute n angle relative to start angle.
					relativeDivisionAngle = divisonAngle * i + control.ScaleStartAngle;

					for (int j = 1; j < control.Subdivisions; j++)
					{
						// Compute n angle relative to start angle.
						relativeSubdivisionAngle = relativeDivisionAngle + (j * subdivisionAngle);
						points[index] = Transform(Center, (MiddleRectangle.Width / 2) - (SubdivisionLength / 2), relativeSubdivisionAngle);
						index++;
					}
				}

				return points;
			}
		}

		public PointF[] SubdivLinesEnd
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				// Compute one subdivision angle.
				float subdivisionAngle = divisonAngle / (control.Subdivisions);
				float relativeDivisionAngle = 0;
				float relativeSubdivisionAngle = 0;
				int index = 0;
				int size = ((control.Subdivisions - 1) * (control.Divisions - 1)) < 0 ? 0 : (control.Subdivisions - 1) * (control.Divisions - 1);
				PointF[] points = new PointF[size];

				for (int i = 0; i < control.Divisions - 1; i++)
				{
					// Compute n angle relative to start angle.
					relativeDivisionAngle = divisonAngle * i + control.ScaleStartAngle;

					for (int j = 1; j < control.Subdivisions; j++)
					{
						// Compute n angle relative to start angle.
						relativeSubdivisionAngle = relativeDivisionAngle + (j * subdivisionAngle);
						points[index] = Transform(Center, (MiddleRectangle.Width / 2) + (SubdivisionLength / 2), relativeSubdivisionAngle);
						index++;
					}
				}

				return points;
			}
		}

		public PointF[] SubdivTrianglesA
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				// Compute one subdivision angle.
				float subdivisionAngle = divisonAngle / (control.Subdivisions);
				float relativeDivisionAngle = 0;
				float relativeSubdivisionAngle = 0;
				int index = 0;
				int size = ((control.Subdivisions - 1) * (control.Divisions - 1)) < 0 ? 0 : (control.Subdivisions - 1) * (control.Divisions - 1);
				PointF[] points = new PointF[size];

				for (int i = 0; i < control.Divisions - 1; i++)
				{
					// Compute n angle relative to start angle.
					relativeDivisionAngle = divisonAngle * i + control.ScaleStartAngle;

					for (int j = 1; j < control.Subdivisions; j++)
					{
						// Compute n angle relative to start angle.
						relativeSubdivisionAngle = relativeDivisionAngle + (j * subdivisionAngle);
						points[index] = Transform(Center, (MiddleRectangle.Width / 2) + (SubdivisionLength / 2), relativeSubdivisionAngle - (control.Width / subdivisionTriangleAngleDivisor));
						index++;
					}
				}

				return points;
			}
		}

		public PointF[] SubdivTrianglesB
		{
			get
			{
				// Compute one division angle.
				float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
				// Compute one subdivision angle.
				float subdivisionAngle = divisonAngle / (control.Subdivisions);
				float relativeDivisionAngle = 0;
				float relativeSubdivisionAngle = 0;
				int index = 0;
				int size = ((control.Subdivisions - 1) * (control.Divisions - 1)) < 0 ? 0 : (control.Subdivisions - 1) * (control.Divisions - 1);
				PointF[] points = new PointF[size];

				for (int i = 0; i < control.Divisions - 1; i++)
				{
					// Compute n angle relative to start angle.
					relativeDivisionAngle = divisonAngle * i + control.ScaleStartAngle;

					for (int j = 1; j < control.Subdivisions; j++)
					{
						// Compute n angle relative to start angle.
						relativeSubdivisionAngle = relativeDivisionAngle + (j * subdivisionAngle);
						points[index] = Transform(Center, (MiddleRectangle.Width / 2) + (SubdivisionLength / 2), relativeSubdivisionAngle + (control.Width / subdivisionTriangleAngleDivisor));
						index++;
					}
				}

				return points;
			}
		}

		public RectangleF OuterRectangle
		{
			get 
			{
				float offset = (float)control.Width / outerRectDivisor / 2f;
				return new RectangleF(Center.X - offset, Center.Y - offset, 2f * offset, 2f * offset);
			}
		}

		public RectangleF MiddleRectangle
		{
			get
			{
				if (!control.OuterDigits && control.InnerDigits)
				{
					float offset = (float)control.Width / middleRectOnlyInnerDivisor / 2f;
					return new RectangleF(Center.X - offset, Center.Y - offset, 2f * offset, 2f * offset);
				}
				else
				{
					float offset = (float)control.Width / middleRectDivisor / 2f;
					return new RectangleF(Center.X - offset, Center.Y - offset, 2f * offset, 2f * offset);
				}
				
			}
		}

		public RectangleF CenterRectangle
		{
			get 
			{
				float offset = (float)control.Width / centerRectDivisor / 2f;
				return new RectangleF(Center.X - offset, Center.Y - offset, 2f * offset, 2f * offset);
			}
		}

		public float OuterThickness
		{
			get { return ((float)control.Width / outerThicknessDivisor); }
		}

		public float PointerThickness
		{
			get { return ((float)control.Width / pointerThicknessDivisor); }
		}

		public float DivisionThickness
		{
			get { return ((float)control.Width / divisionThicknessDivisor); }
		}

		public float DivisionLength
		{
			get { return ((float)control.Width / divisionLengthDivisor); }
		}

		public float[] DivisionValues
		{
			get 
			{
				float[] values = new float[control.Divisions];
				float scaleValue = (control.MaxValue - control.MinValue) / (float)control.Divisions;
				float currentValue = control.MinValue;

				for (int i = 0; i < values.Length; i++)
				{
					values[i] = currentValue;
					currentValue += scaleValue;
				}

				return values;
			}
		}

		public float SubdivisionThickness
		{
			get { return ((float)control.Width / subdivisionThicknessDivisor); }
		}

		public float SubdivisionLength
		{
			get { return ((float)control.Width / subdivisionLengthDivisor); }
		}

		public float DigitsSize
		{
			get { return (float)control.Width / digitsSizeDivisor; }
		}
		
		#endregion		
		
		#region Methods

		private float DegreeToRadian(float degree)
		{
			return degree * (float)Math.PI / straightAngel;
		}

		private float RadianToDegree(float radians)
		{
			return radians * (float)Math.PI / straightAngel;
		}

		private float LineLength(PointF first, PointF second)
		{
			float dx = second.X - first.X;
			float dy = second.Y - first.Y;

			return (float)Math.Sqrt(Pow2(dx) + Pow2(dy));
		}

		private float Pow2(float value)
		{
			return (float)Math.Pow(value, 2);
		}

		private PointF Transform(PointF point, float length, float angle)
		{
			float x = point.X + length * (float)Math.Cos((double)DegreeToRadian(angle));
			float y = 0f;

			//  Take into consideration rotation direction.
			switch (control.RotationDirection)
			{
				case RotationDirection.Clockwise:
					y = point.Y + length * (float)Math.Sin((double)DegreeToRadian(angle));
					break;
				case RotationDirection.Anticlockwise:
					y = point.Y - length * (float)Math.Sin((double)DegreeToRadian(angle));
					break;
			}

			return new PointF(x, y);
		}

		private PointF ComputeOuterDigitPos(PointF point, float angle, SizeF size)
		{
			// Compute startAngle and stopAngle rotation count. 
			int rotationCount = (int)(angle / fullAngel);
			angle = angle - (rotationCount * fullAngel);
			int divisionAngle = 90;
			// Compute in which part current angle is included.
			int circlePart = (int)((angle + divisionAngle / 2) / divisionAngle);
			// Compute angle percentage.
			float anglePercentage = (angle - (circlePart * divisionAngle - (divisionAngle / 2))) / divisionAngle;

			// Depend on which part ange is contained it offsets the start point of rectangle.
			if (circlePart >= 1 && circlePart < 2)
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Width * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Width - firstOffset;
				return new PointF(point.X - firstOffset, point.Y);
			}
			else if (circlePart >= 2 && circlePart < 3)
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Height * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Height - firstOffset;
				return new PointF(point.X - size.Width, point.Y - firstOffset);
			}
			else if (circlePart >= 3 && circlePart < 4)
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Width * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Width - firstOffset;
				return new PointF(point.X - secondOffset, point.Y - size.Height);
			}
			else
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Height * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Height - firstOffset;
				return new PointF(point.X, point.Y - secondOffset);
			}
		}

		private PointF ComputeInnerDigitPos(PointF point, float angle, SizeF size)
		{
			// Compute startAngle and stopAngle rotation count. 
			int rotationCount = (int)(angle / fullAngel);
			angle = angle - (rotationCount * fullAngel);
			int divisionAngle = 90;
			// Compute in which part current angle is included.
			int circlePart = (int)((angle + divisionAngle / 2) / divisionAngle);
			// Compute angle percentage.
			float anglePercentage = (angle - (circlePart * divisionAngle - (divisionAngle / 2))) / divisionAngle;

			// Depend on which part ange is contained it offsets the start point of rectangle.
			if (circlePart >= 1 && circlePart < 2)
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Width * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Width - firstOffset;
				return new PointF(point.X - secondOffset, point.Y - size.Height);
			}
			else if (circlePart >= 2 && circlePart < 3)
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Height * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Height - firstOffset;
				return new PointF(point.X, point.Y - secondOffset);
			}
			else if (circlePart >= 3 && circlePart < 4)
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Width * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Width - firstOffset;
				return new PointF(point.X - firstOffset, point.Y);
			}
			else
			{
				// Compute firts offset based on angle percentage.
				float firstOffset = size.Height * anglePercentage;
				// Compute second offset based on angle percentage.
				float secondOffset = size.Height - firstOffset;
				return new PointF(point.X - size.Width, point.Y - firstOffset);
			}
		}

		public PointF GetOuterDigitPos(int index, SizeF size)
		{
			float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
			float relativeAngle = divisonAngle * index + control.ScaleStartAngle;
			PointF startPoint = Transform(Center, (MiddleRectangle.Width / 2) + (control.Width / digitsOffsetDivisor), relativeAngle);

			return ComputeOuterDigitPos(startPoint, relativeAngle, size);
		}

		public PointF GetInnerDigitPos(int index, SizeF size)
		{
			float divisonAngle = (control.ScaleStopAngle - control.ScaleStartAngle) / (control.Divisions - 1);
			float relativeAngle = divisonAngle * index + control.ScaleStartAngle;
			PointF startPoint = Transform(Center, (MiddleRectangle.Width / 2) - (control.Width / digitsOffsetDivisor), relativeAngle);

			return ComputeInnerDigitPos(startPoint, relativeAngle, size);
		}

		#endregion	
	}
}
