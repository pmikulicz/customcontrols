﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CustomControls.Gauges.RadialGauges
{
	/// <summary>
	/// Speed Gauge Control - A Windows User Control.
	/// Author  : Patryk Mikulicz
	/// Date    : 11 October 1013
	/// email   : patryk.mikulicz@gmail.com
	/// This is control is for free. You can use for any commercial or non-commercial purposes.
	/// [Please do no remove this header when using this control in your application.]
	/// </summary>
	public partial class RadialGauge : UserControl
	{

		#region Fields

		// Buffer for foregound layer [pointer].
		private Bitmap foregroundBuffer;
		// Buffer for backgorund layer [background].
		private Bitmap backgroundBuffer;
		// Buffer for middleground layer [divisions and digits].
		private Bitmap middlegroundBuffer;

		#region Default Values
		private readonly Color defaultFrontColor = Color.White;
		private readonly Color defaultPointerColor = Color.Red;
		private readonly Color defaultBackColor = Color.Black;
		private readonly Color defaultDigitsColor = Color.White;
		private readonly string defaultDigitsFont = "Arial";
		private const DivisionStyle defaultDivisionStyle = DivisionStyle.Linear;
		private const DivisionStyle defaultSubdivisionStyle = DivisionStyle.Linear;
		private const PointerStyle defaultPointerStyle = PointerStyle.Triangular;
		private const RotationDirection defaultRotationDirection = RotationDirection.Clockwise;
		private const int defaultWidth = 300;
		private const int defaultHeigh = 300;
		private const int defaultDivisions = 10;
		private const int defaultSubdivisions = 3;
		private const int minimumWidth = 50;
		private const int minimumHeigh = 50;
		private const float defaultMaxValue = 200f;
		private const float defaultMinValue = 0f;
		private const bool defaultOuterDigits = true;
		private const bool defaultInnerDigits = true;
		private const float defaultScaleStartAngle = 135f;
		private const float defaultScaleStopAngle = 405f;
		#endregion
		private Gauge gauge;
		private float minValue;
		private float maxValue;
		private float currentValue;
		private float scaleStartAngle;
		private float scaleStopAngle;
		private bool outerDigits;
		private bool innerDigits;
		private int divisions;
		private int subdivisions;
		private DivisionStyle divisionStyle;
		private DivisionStyle subdivisionStyle;
		private PointerStyle pointerStyle;
		private RotationDirection rotationDirection;
		private Color frontColor;
		private Color pointerColor;
		private Color backColor;
		private Color divisionsColor;
		private Color subdivisionsColor;
		private Color digitsColor;
		private string digitsFont;

		#endregion

		#region Constructors

		public RadialGauge()
		{
			gauge = new Gauge(this);
			InitializeComponent();
			DefaultSetup();

			// Set the control style to double buffer.
			SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.SupportsTransparentBackColor | ControlStyles.OptimizedDoubleBuffer, true);
			UpdateStyles();

			RecreateBuffers();
			Draw(Layer.All);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets minimum value on the scale.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(0)]
		[Description("Mininum value on the scale")]
		public float MinValue
		{
			get 
			{
				return minValue; 
			}
			set 
			{
				if (value < MaxValue && value != minValue)
				{
					minValue = value;

					if (CurrentValue < minValue)
						CurrentValue = minValue;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets maximum value on the scale.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(100)]
		[Description("Maximum value on the scale")]
		public float MaxValue
		{
			get
			{
				return maxValue;
			}
			set
			{
				if (value > MinValue && value != maxValue)
				{
					maxValue = value;

					if (CurrentValue > maxValue)
						CurrentValue = maxValue;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets current value on the scale.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(0)]
		[Description("Current value on the scale")]
		public float CurrentValue
		{
			get
			{
				return currentValue;
			}
			set
			{
				if (value >= MinValue && value <= MaxValue && value != currentValue)
				{
					currentValue = value;
					Draw(Layer.Front);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets number of divisions on the scale.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(10)]
		[Description("Number of divisions on the scale")]
		public int Divisions
		{
			get
			{
				return divisions;
			}
			set
			{
				if (value >= 0 & value != divisions)
				{
					divisions = value;
					Draw(Layer.Middle);
					Invalidate();
				}				
			}
		}

		/// <summary>
		/// Gets or sets start angle of scale on the scale.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(10)]
		[Description("Start angle of scale on the scale")]
		public float ScaleStartAngle
		{
			get
			{
				return scaleStartAngle;
			}
			set
			{
				// Check if start angle isn't greater than stop angle
				if (value != scaleStartAngle)
				{
					scaleStartAngle = value;

					Draw(Layer.Middle);
					Draw(Layer.Front);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets start angle of scale on the scale.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(10)]
		[Description("Stop angle of scale on the scale")]
		public float ScaleStopAngle
		{
			get
			{
				return scaleStopAngle;
			}
			set
			{
				// Check if start angle isn't lower than stop angle
				if (value != scaleStopAngle)
				{
					scaleStopAngle = value;

					Draw(Layer.Middle);
					Draw(Layer.Front);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets number of subdivisions on the scale.
		/// </summary>
		[Browsable(true)]
		[DefaultValue(3)]
		[Description("Number of subdivisions on the scale")]
		public int Subdivisions
		{
			get
			{
				return subdivisions;
			}
			set
			{
				if (value >= 0 && value != subdivisions)
				{
					subdivisions = value;
					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets the front color of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Front color of the gauge")]
		public Color FrontColor
		{
			get
			{
				return frontColor;
			}
			set
			{
				if (value != frontColor)
				{
					frontColor = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets the divisions color of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Divisions color of the gauge")]
		public Color DivisionsColor
		{
			get
			{
				return divisionsColor;
			}
			set
			{
				if (value != divisionsColor)
				{
					divisionsColor = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets the subdivisions color of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Subdivisions color of the gauge")]
		public Color SubdivisionsColor
		{
			get
			{
				return subdivisionsColor;
			}
			set
			{
				if (value != subdivisionsColor)
				{
					subdivisionsColor = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets the pointer color of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Pointer color of the gauge")]
		public Color PointerColor
		{
			get
			{
				return pointerColor;
			}
			set
			{
				if (value != pointerColor)
				{
					pointerColor = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets the back color of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Back color of the gauge")]
		public new Color BackColor
		{
			get
			{
				return backColor;
			}
			set
			{
				if (value != backColor)
				{
					backColor = value;

					Draw(Layer.Back);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets the digits color of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Digits color of the gauge")]
		public Color DigitsColor
		{
			get
			{
				return digitsColor;
			}
			set
			{
				if (value != digitsColor)
				{
					digitsColor = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets the digits font of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Digits font of the gauge")]
		public string DigitsFont
		{
			get
			{
				return digitsFont;
			}
			set
			{
				if (value != digitsFont)
				{
					digitsFont = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets outer digits of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Outer digits of the gauge")]
		public bool OuterDigits
		{
			get
			{
				return outerDigits;
			}
			set
			{
				if (value != outerDigits)
				{
					outerDigits = value;

					Draw(Layer.Middle);
					Draw(Layer.Front);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets inner digits of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Inner digits of the gauge")]
		public bool InnerDigits
		{
			get
			{
				return innerDigits;
			}
			set
			{
				if (value != innerDigits)
				{
					innerDigits = value;

					Draw(Layer.Middle);
					Draw(Layer.Front);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets division style of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Division style of the gauge")]
		public DivisionStyle DivisionStyle
		{
			get
			{
				return divisionStyle;
			}
			set
			{
				if (value != divisionStyle)
				{
					divisionStyle = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets subdivision style of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Division style of the gauge")]
		public DivisionStyle SubdivisionStyle
		{
			get
			{
				return subdivisionStyle;
			}
			set
			{
				if (value != subdivisionStyle)
				{
					subdivisionStyle = value;

					Draw(Layer.Middle);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets pointer style of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Pointer style of the gauge")]
		public PointerStyle PointerStyle
		{
			get
			{
				return pointerStyle;
			}
			set
			{
				if (value != pointerStyle)
				{
					pointerStyle = value;

					Draw(Layer.Front);
					Invalidate();
				}
			}
		}

		/// <summary>
		/// Gets or sets rotation direction of the gauge.
		/// </summary>
		[Browsable(true)]
		[Description("Rotation direction of the gauge")]
		public RotationDirection RotationDirection
		{
			get
			{
				return rotationDirection;
			}
			set
			{
				if (value != rotationDirection)
				{
					rotationDirection = value;

					Draw(Layer.Middle);
					Draw(Layer.Front);
					Invalidate();
				}
			}
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Sets up default values for control.
		/// </summary>
		private void DefaultSetup()
		{
			// set up directly private fields to avoid control invalidation.
			backColor = defaultBackColor;
			frontColor = defaultFrontColor;
			divisionsColor = defaultFrontColor;
			subdivisionsColor = defaultFrontColor;
			pointerColor = defaultPointerColor;
			digitsColor = defaultDigitsColor;
			outerDigits = defaultOuterDigits;
			innerDigits = defaultInnerDigits;
			divisions = defaultDivisions;
			scaleStartAngle = defaultScaleStartAngle;
			scaleStopAngle = defaultScaleStopAngle;
			subdivisions = defaultSubdivisions;
			divisionStyle = defaultDivisionStyle;
			subdivisionStyle = defaultSubdivisionStyle;
			pointerStyle = defaultPointerStyle;
			maxValue = defaultMaxValue;
			minValue = defaultMinValue;
			currentValue = defaultMinValue;
			digitsFont = defaultDigitsFont;
			
			Size = new Size(defaultWidth, defaultHeigh);
			MinimumSize = new Size(minimumWidth, minimumHeigh);
		}

		/// <summary>
		/// Draw specified layers to the backbuffers.
		/// </summary>
		/// <param name="layer">Layer to be drawn</param>
		private void Draw(Layer layer)
		{
			switch (layer)
			{
				case Layer.Back:
					DrawBack();
					break;
				case Layer.Middle:
					DrawMiddle();
					break;
				case Layer.Front:
					DrawFront();
					break;
				case Layer.All:
					DrawBack();
					DrawMiddle();
					DrawFront();
					break;
			}
		}

		/// <summary>
		/// Draw front layer for foreground buffer.
		/// </summary>
		private void DrawFront()
		{
			lock (foregroundBuffer)
			{
				using (Graphics graphics = Graphics.FromImage(foregroundBuffer))
				{
					graphics.Clear(Color.Transparent);
					graphics.SmoothingMode = SmoothingMode.HighQuality;
					// Draw Elements
					DrawPointer(graphics);
					
				}
			}
		}

		/// <summary>
		/// Draw middle layer for middleground buffer.
		/// </summary>
		private void DrawMiddle()
		{
			lock (middlegroundBuffer)
			{
				using (Graphics graphics = Graphics.FromImage(middlegroundBuffer))
				{
					graphics.Clear(Color.Transparent);
					graphics.SmoothingMode = SmoothingMode.HighQuality;
					// Draw elements.
					DrawDivisions(graphics);
					DrawDigits(graphics);
				}
			}
		}

		/// <summary>
		/// Draw back layer for background buffer.
		/// </summary>
		private void DrawBack()
		{
			lock (backgroundBuffer)
			{
				using (Graphics graphics = Graphics.FromImage(backgroundBuffer))
				{
					graphics.Clear(Color.Transparent);
					graphics.SmoothingMode = SmoothingMode.HighQuality;

					graphics.FillEllipse(new SolidBrush(BackColor), gauge.OuterRectangle);
					graphics.DrawEllipse(new Pen(new SolidBrush(ControlPaint.Light(BackColor)), gauge.OuterThickness), gauge.OuterRectangle);
				}
			}
		}

		/// <summary>
		/// Draws divisions on the gauge.
		/// </summary>
		/// <param name="graphics"></param>
		private void DrawDivisions(Graphics graphics)
		{
			// Create local vars to avoid loops during using getter property.
			PointF[] divs = gauge.Divisions;
			PointF[] divsLS = gauge.DivLinesStart;
			PointF[] divsLE = gauge.DivLinesEnd;
			PointF[] divsTA= gauge.DivTrianglesA;
			PointF[] divsTB = gauge.DivTrianglesB;
			PointF[] subdivs = gauge.Subdivisions;
			PointF[] subdivsLS = gauge.SubdivLinesStart;
			PointF[] subdivsLE = gauge.SubdivLinesEnd;
			PointF[] subdivsTA = gauge.SubdivTrianglesA;
			PointF[] subdivsTB = gauge.SubdivTrianglesB;
			

			for (int i = 0; i < gauge.Divisions.Length; i++)
			{
				switch (DivisionStyle)
				{
					case DivisionStyle.Dotted:
						graphics.FillEllipse(new SolidBrush(DivisionsColor), new RectangleF(divs[i].X - gauge.DivisionLength / 2, divs[i].Y - gauge.DivisionLength / 2, gauge.DivisionLength, gauge.DivisionLength));
						break;
					case DivisionStyle.Linear:
						graphics.DrawLine(new Pen(new SolidBrush(DivisionsColor), gauge.DivisionThickness), divsLS[i], divsLE[i]);
						break;
					case DivisionStyle.Triangular:
						graphics.FillPolygon(new SolidBrush(DivisionsColor), new PointF[] { divsLS[i], divsTA[i], divsTB[i] });
						break;
				}
			}

			for (int i = 0; i < gauge.Subdivisions.Length; i++)
			{
				switch (SubdivisionStyle)
				{
					case DivisionStyle.Dotted:
						graphics.FillEllipse(new SolidBrush(SubdivisionsColor), new RectangleF(subdivs[i].X - gauge.SubdivisionLength / 2, subdivs[i].Y - gauge.SubdivisionLength / 2, gauge.SubdivisionLength, gauge.SubdivisionLength));
						break;
					case DivisionStyle.Linear:
						graphics.DrawLine(new Pen(new SolidBrush(SubdivisionsColor), gauge.SubdivisionThickness), subdivsLS[i], subdivsLE[i]);
						break;
					case DivisionStyle.Triangular:
						graphics.FillPolygon(new SolidBrush(SubdivisionsColor), new PointF[] { subdivsLS[i], subdivsTA[i], subdivsTB[i] });
						break;
				}
			}
		}

		/// <summary>
		/// Draws digits on the gauge.
		/// </summary>
		/// <param name="graphics"></param>
		private void DrawDigits(Graphics graphics)
		{
			Font font = new Font(digitsFont, gauge.DigitsSize, FontStyle.Bold);
			int length = gauge.Divisions.Length;
			float[] divisionsValues = gauge.DivisionValues;

			for (int i = 0; i < length; i++)
			{
				string value = divisionsValues[i].ToString();
				SizeF stringSize = graphics.MeasureString(value, font);

				if (OuterDigits)
					graphics.DrawString(value, font, new SolidBrush(DigitsColor), gauge.GetOuterDigitPos(i, stringSize));

				if (InnerDigits)
					graphics.DrawString(value, font, new SolidBrush(DigitsColor), gauge.GetInnerDigitPos(i, stringSize));
			}
		}

		/// <summary>
		/// Draws pointer on the gauge.
		/// </summary>
		/// <param name="graphics"></param>
		private void DrawPointer(Graphics graphics)
		{
			graphics.DrawLine(new Pen(new SolidBrush(Color.Red), gauge.PointerThickness), gauge.Center, gauge.Pointer);
			graphics.FillEllipse(new SolidBrush(ControlPaint.Light(BackColor)), gauge.CenterRectangle);
		}

		/// <summary>
		/// Recreates backbuffers in case of control resize.
		/// </summary>
		private void RecreateBuffers()
		{
			if (backgroundBuffer != null)
				backgroundBuffer.Dispose();

			if (middlegroundBuffer != null)
				middlegroundBuffer.Dispose();

			if (foregroundBuffer != null)
				foregroundBuffer.Dispose();

			backgroundBuffer = new Bitmap(Width, Height, PixelFormat.Format32bppPArgb);
			middlegroundBuffer = new Bitmap(Width, Height, PixelFormat.Format32bppPArgb);
			foregroundBuffer = new Bitmap(Width, Height, PixelFormat.Format32bppPArgb);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
					components.Dispose();

				if (foregroundBuffer != null)
					foregroundBuffer.Dispose();

				if (middlegroundBuffer != null)
					middlegroundBuffer.Dispose();

				if (backgroundBuffer != null)
					backgroundBuffer.Dispose();
			}

			base.Dispose(disposing);
		}

		/// <summary>
		/// Overrides OnPaint event which draws control from foreground buffer.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPaint(PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			lock (backgroundBuffer) { e.Graphics.DrawImage(backgroundBuffer, Point.Empty); }
			lock (backgroundBuffer) { e.Graphics.DrawImage(middlegroundBuffer, Point.Empty); }
			lock (backgroundBuffer) { e.Graphics.DrawImage(foregroundBuffer, Point.Empty); }
		}

		/// <summary>
		/// Overrides OnResize event which recreates backbuffers and draw both layers.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnResize(EventArgs e)
		{
			// Ensure the control remains square (Height = Width).
			if (Height != Width)
				Size = new Size(Height, Height);

			RecreateBuffers();
			Draw(Layer.All);			
		}

		#endregion
	}

	public enum DivisionStyle
	{
		Triangular,
		Dotted,
		Linear
	}

	public enum PointerStyle
	{
		Triangular,
		Straight,
		Sharp
	}

	public enum RotationDirection
	{
		Clockwise,
		Anticlockwise
	}

	internal enum Layer
	{
		Front,
		Back,
		Middle,
		All
	}
}
